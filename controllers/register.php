<?php
/**
 * Register.php - Register controller. Adds new users to the database
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';
include_once dirname ( __FILE__ ) . '/../components/datatools.php';
include_once dirname ( __FILE__ ) . '/../components/formvalidationtools.php';

global $action; // set in index.php
                
// Determine and process the action.
switch ($action) {
	case "save" :
		register_user ();
		header ( "Location: index.php" );
		break;
	default :
		die ( "Illegal action" );
}

/**
 * Adds a user to the database..
 */
function register_user() {
	global $mysqli;
	// Step 1: check is all required fields are sent
	if (! isset ( $_POST ['Userid'] ) || ! isset ( $_POST ['Password'] ) || ! isset ( $_POST ['PwdRetype'] ) || ! isset ( $_POST ['Name_first'] ) || ! isset ( $_POST ['Name_middle'] ) || ! isset ( $_POST ['Name_last'] ) || ! isset ( $_POST ['Email'] ) || ! isset ( $_POST ['Telephone'] ) || ! isset ( $_POST ['Address_street'] ) || ! isset ( $_POST ['Address_number'] ) || ! isset ( $_POST ['Address_zipcode'] ) || ! isset ( $_POST ['Address_city'] )) {
		die ( 'Error in form' );
	}
	
	// step 2: safely transfer the post-data to local variables
	$userid = strip_tags ( $_POST ['Userid'] );
	$password = strip_tags ( $_POST ['Password'] );
	$pwd_retype = strip_tags ( $_POST ['PwdRetype'] );
	$name_first = strip_tags ( $_POST ['Name_first'] );
	$name_middle = strip_tags ( $_POST ['Name_middle'] );
	$name_last = strip_tags ( $_POST ['Name_last'] );
	$phone = strip_tags ( $_POST ['Telephone'] );
	$email = strip_tags ( $_POST ['Email'] );
	$address_street = strip_tags ( $_POST ['Address_street'] );
	$address_number = strip_tags ( $_POST ['Address_number'] );
	$address_zipcode = strip_tags ( $_POST ['Address_zipcode'] );
	$address_city = strip_tags ( $_POST ['Address_city'] );
	
	// Step 3: Now, the data must be validated. I used the functions declared in formvalidationtools.php and datatools.php
	// filling $error_message with text when something is wrong
	$error_message = "";
	$error_message = "";
	$error_message .= validateLength ( $userid, 3, 'De userid is te kort.' );
	if (useridExists ( $userid )) {
		$error_message .= 'De userid bestaat al.<br />';
	}
	$error_message .= validateLength ( $password, 4, 'Het wachtwoord is te kort.' );
	if ($password != $pwd_retype) {
		$error_message .= 'De wachtwoorden zijn niet gelijk';
	}
	$error_message .= validateCharacters ( $name_first, 'De voornaam is niet valide.' );
	$error_message .= validateCharacters ( $name_last, 'De achternaam is niet valide.' );
	$error_message .= validateEmail ( $email, 'Het email adres is niet valide' );
	if (emailExistsInUser ( $email )) {
		$error_message .= 'Dit email adres bestaat al.<br/>';
	}
	
	// Validation has errors when the length of error_message > 0
	if (strlen ( $error_message ) > 0) {
		die ( $error_message );
	}
	
	// Step 4: all is well. Insert a new user to the database
	$password = hashPassword ( $password );
	$sql = "INSERT INTO USER (`Userid`, `Password`, `Name_first`, `Name_middle`, `Name_last`, `Email`, `Telephone`, " . "`Adress_street`, `Adress_number`, `Adress_zipcode`, `Adress_city`) " . "VALUES ('$userid', '$password', '$name_first', '$name_middle', '$name_last', '$email', '$phone', " . "'$address_street', '$address_number', '$address_zipcode', '$address_city');";
	if (! $mysqli->query ( $sql )) {
		die ( "Errormessage: " . $mysqli->error );
	}
}
?>